<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>view hangars</title>
<link rel="stylesheet" type="text/css" href="./resources/css/style.css"/>
</head>
<body>
<div id="div">
<h1>Hangar Details</h1>
<p align="right"><a href="index.jsp">Home</a></p>
<table border="5" id="table">
<tr><th>PlaneId</th><th>HangarName</th><th>HangarCapacity</th><th>HangarLocation</th><th>Status</th><th>Update</th></tr>
<c:forEach items="${hangars}" var="h">
<tr>
<td><a href="hangardetails?planeId=${h.getPlaneId()}">${h.getPlaneId()}</a></td>
<td>${h.getHangarName()}</td>
<td>${h.getHangarCapacity()}</td>
<td>${h.getHangarLocation()}</td>
<td>${h.getStatus()}</td>
<td><a href="edithangar?planeId=${p.getPlaneId()}">update</a></td>
</tr>
</c:forEach>
</table>
</div>
</body>
</html>