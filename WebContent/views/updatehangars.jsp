<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>update hangars</title>
<link rel="stylesheet" type="text/css" href="./resources/css/style.css"/>
</head>
<body>
<div>
<h1>Update Hangars </h1>
<p align="right"><a href="index.jsp">Home</a></p>
<f:form action="updatehangar" method="post" modelAttribute="h">
<table>
<tr><td>HangarName</td><td><f:input path="hangarName"/></td></tr>
<tr><td>HangarCapacity</td><td><f:input path="hangarCapacity" /></td></tr>
<tr><td>HangarLocation</td><td><f:input path="hangarLocation"/></td></tr>
<tr><td>Status</td><td><f:input path="status"/></td></tr>
<tr><td></td><td><input type="submit" value="Update"></td></tr> 
</table>
</f:form>
</div>
</body>
</html>