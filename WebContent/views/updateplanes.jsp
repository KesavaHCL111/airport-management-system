<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>updateplane</title>
<link rel="stylesheet" type="text/css" href="./resources/css/style.css"/>
</head>
<body>
<div>
<h1>Update Planes </h1>
<p align="right"><a href="index.jsp">Home</a></p>
<f:form action="updateplane" method="post" modelAttribute="u">
<table>
<tr><td>PlaneName</td><td><f:input path="planeName"/></td></tr>
<tr><td>Source</td><td><f:input path="source" /></td></tr>
<tr><td>Destination</td><td><f:input path="destination"/></td></tr>
<tr><td></td><td><input type="submit" value="Update"></td></tr> 
</table>
</f:form>
</div>
</body>
</html>