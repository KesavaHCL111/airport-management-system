<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>update pilot</title>
<link rel="stylesheet" type="text/css" href="./resources/css/style.css"/>
</head>
<body>
<div>
<h1>Update Pilot </h1>
<p align="right"><a href="index.jsp">Home</a></p>
<f:form action="updatepilot" method="post" modelAttribute="u">
<table>
<tr><td>PilotName</td><td><f:input path="pilotName"/></td></tr>
<tr><td>Age</td><td><f:input path="age"/></td></tr>
<tr><td>Gender</td><td><f:radiobutton path="gender"/>Male &nbsp; <f:radiobutton path="gender"/>Female</td></tr>
<tr><td>ContactNumber</td><td><f:input path="contactNumber"/></td></tr>
<tr><td></td><td><input type="submit" value="Update"></td></tr>
</table>
</f:form>
</div>
</body>
</html>