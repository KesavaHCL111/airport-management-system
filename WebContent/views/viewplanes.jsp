<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>view planes</title>
<link rel="stylesheet" type="text/css" href="./resources/css/style.css"/>
</head>
<body>
<div id="div">
<h1>Plane Details</h1>
<p align="right"> <a href="index.jsp">Home</a></p>
<table border="5" id="table">
<tr><th>PlaneId</th><th>PlaneName</th><th>Source</th><th>Destination</th><th>NumberOfSeats</th><th>Update</th></tr>
<c:forEach items="${planes}" var="p">
<tr>
<td><a href="planedetails?planeId=${p.getPlaneId()}">${p.getPlaneId()}</a></td>
<td>${p.getPlaneName() }</td>
<td>${p.getSource() }</td>
<td>${p.getDestination()}</td>
<td>${p.getNumberOfSeats() }</td>
<td><a href="editplane?planeId=${p.getPlaneId()}">update</a></td>
</tr>
</c:forEach>
</table>
</div>
</body>
</html>