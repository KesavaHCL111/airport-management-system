<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>pilotId details</title>
<link rel="stylesheet" type="text/css" href="./resources/css/style.css"/>
</head>
<body>
<div id="div">
	<h1>Pilot Details</h1>
	<!-- Pilot Details by clicking Id -->
	<table border="5" id="table">
		<tr>
			<th>PilotId</th>
			<th>PilotName</th>
			<th>Age</th>
			<th>Gender</th>
			<th>ContactNumber</th>
		</tr>
		<c:forEach items="${pilots}" var="p">
			<tr>
				<td>${p.getPilotId()}</td>
				<td>${p.getPilotName() }</td>
				<td>${p.getAge() }</td>
				<td>${p.getGender()}</td>
				<td>${p.getContactNumber() }</td>
			</tr>
		</c:forEach>
	</table>
	</div>
</body>
</html>