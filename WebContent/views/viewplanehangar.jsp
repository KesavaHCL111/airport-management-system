<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>plane hangar</title>
</head>
<body>
<h2>Plane and Hangar Details</h2>
<table>
<tr><th>PlaneId</th><th>PlaneName</th><th>HangarName</th><th>Status</th></tr>
<tr>
<c:forEach items="planehangars" var="ph" >
<td>${ph.getPlaneId()}</td>
<td>${ph.getPlaneName()}</td>
<td>${ph.getHangarName() }</td> 
<td>${ph.getStatus()}</td>
</c:forEach>
</tr>
</table>
</body>
</html>