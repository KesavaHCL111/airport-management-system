package com.airport.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.airport.dao.IAdminDao;
import com.airport.dao.IAdminDaoImpl;
import com.airport.dao.IManagerDao;
import com.airport.dao.IManagerDaoImpl;
import com.airport.model.Admin;
import com.airport.model.Hangar;
import com.airport.model.Manager;
import com.airport.model.Pilot;
import com.airport.model.Plane;

@Controller
public class AirportController {
	IAdminDao dao = new IAdminDaoImpl();
	IManagerDao mdao = new IManagerDaoImpl();

	@RequestMapping("register")
	public ModelAndView insertAdmin() {
		return new ModelAndView("adminregistration", "a", new Admin());
	}

	@RequestMapping("addadmin")
	public ModelAndView add(@ModelAttribute("a") Admin admin) {
		dao.insertAdmin(admin);
		return new ModelAndView("display", "d", admin);
	}

	@RequestMapping("managerregister")
	public ModelAndView insertManager() {
		return new ModelAndView("managerregistration", "m", new Manager());
	}

	@RequestMapping("addmanager")
	public ModelAndView addManager(@ModelAttribute("m") Manager manager) {
		mdao.inserManager(manager);
		return new ModelAndView("displaymanager", "dm", manager);
	}

	@RequestMapping("adminlogin")
	public ModelAndView adminValid() {
		return new ModelAndView("adminlogin", "a", new Admin());
	}

	@RequestMapping("adminvalid")
	public ModelAndView adminLoginValid(@ModelAttribute("a") Admin admin) {
		boolean isValid = dao.adminAthentication(admin);
		if (isValid) {
			return new ModelAndView("adminvalid", "av", "Login Succesfully");
		} else {
			return new ModelAndView("adminlogin", "av", "Invalid Credentials");
		}
	}

	@RequestMapping("managerlogin")
	public ModelAndView managerValid() {
		return new ModelAndView("managerlogin", "m", new Manager());
	}

	@RequestMapping("managervalid")
	public ModelAndView managerLoginValid(@ModelAttribute("m") Manager manager) {
		boolean isValid = mdao.managerAthentication(manager);
		if (isValid) {
			return new ModelAndView("managervalid", "mv", "Login Successfully");
		} else {
			return new ModelAndView("managerlogin", "mv", "Invalid Credentials");
		}
	}

	@RequestMapping("addplane")
	public ModelAndView addPlane() {
		return new ModelAndView("addplanes", "a", new Pilot());
	}

	@RequestMapping("inserplane")
	public ModelAndView insertPlane(@ModelAttribute("p") Pilot pilot) {
		dao.insertPlane(pilot);
		return new ModelAndView("addplanedisplay", "p", "Your details are submitted succesfully");
	}

	@RequestMapping("viewplane")
	public ModelAndView viewPlane() {
		List<Plane> list = dao.viewPlanes();
		return new ModelAndView("viewplanes", "planes", list);
	}

	@RequestMapping("editplane")
	public ModelAndView editPlane() {
		return new ModelAndView("updateplanes", "u", new Plane());
	}

	@RequestMapping("updateplane")
	public ModelAndView updatePlane(@ModelAttribute("u") Plane plane) {
		dao.updatePlane(plane);
		return new ModelAndView("addplanedisplay", "u", "Plane Deatials Updated successfully");
	}

	@RequestMapping("planedetails")
	public ModelAndView plainDetailsById(@ModelAttribute("planes") Plane plane) {
		List<Plane> list = dao.viewPlanesById(plane);
		return new ModelAndView("planeiddetails", "planes", list);
	}

	/*
	 * @RequestMapping("addpilot") public ModelAndView addPilot() { return new
	 * ModelAndView("addplanes", "u", new Pilot()); }
	 * 
	 * @RequestMapping("insertpilot") public ModelAndView
	 * inserPlane(@ModelAttribute("p") Pilot pilot) { dao.insertPilot(pilot); return
	 * new ModelAndView("addplanedisplay", "pilot",
	 * "Your details are submitted succesfully"); }
	 */
//Pilot
	@RequestMapping("viewpilot")
	public ModelAndView viewPilots() {
		List<Pilot> list = dao.viewPilots();
		return new ModelAndView("viewpilots", "pilots", list);
	}

	@RequestMapping("editpilot")
	public ModelAndView editPilot() {
		return new ModelAndView("updatepilots", "u", new Pilot());
	}

	@RequestMapping("updatepilot")
	public ModelAndView updatePilot(@ModelAttribute("u") Pilot pilot) {
		dao.updatePilot(pilot);
		return new ModelAndView("addplanedisplay", "u", "Pilot Deatials Updated successfully");
	}

	@RequestMapping("pilotdetails")
	public ModelAndView pilotDetailsById(@ModelAttribute("pilots") Pilot pilot) {
		List<Pilot> list = dao.viewPilotById(pilot);
		return new ModelAndView("pilotiddetails", "pilots", list);
	}

//Hangar
	@RequestMapping("addhangar")
	public ModelAndView addHangar() {
		return new ModelAndView("addhangars", "h", new Hangar());
	}

	@RequestMapping("inserthangar")
	public ModelAndView insertHangar(@ModelAttribute("h") Hangar hangar) {
		dao.insertHangar(hangar);
		return new ModelAndView("addplanedisplay", "h", "Hangar Details Submitted Succesfully");
	}

	@RequestMapping("viewhangar")
	public ModelAndView viewHangar() {
		List<Hangar> list = dao.viewHangars();
		return new ModelAndView("viewhangars", "hangars", list);
	}

	@RequestMapping("hangardetails")
	public ModelAndView hangarDetailsById(@ModelAttribute("hangars") Hangar hangar) {
		List<Hangar> list = dao.viewHangarById(hangar);
		return new ModelAndView("hangariddetails", "hangars", list);
	}

	// Hangar and Plane
	@RequestMapping("allothangar")
	public ModelAndView viewPlaneHangar() {
		List list = mdao.viewPlaneHangar();
		return new ModelAndView("viewplanehangar", "planehangars", list);
	}

	@RequestMapping("edithangar")
	public ModelAndView editHangar() {
		return new ModelAndView("updatehangars", "h", new Hangar());
	}

	@RequestMapping("updatehangar")
	public ModelAndView updateHangar(@ModelAttribute("hg") Hangar hangar) {
		dao.updateHangar(hangar);
		return new ModelAndView("addplanedisplay", "hg", "Hangar Deatials Updated successfully");
	}
	
}
