package com.airport.dao;

import java.util.List;

import com.airport.model.Admin;
import com.airport.model.Hangar;
import com.airport.model.Pilot;
import com.airport.model.Plane;

public interface IAdminDao {
	public void insertAdmin(Admin admin);

	public boolean adminAthentication(Admin admin);

//plane
	public void insertPlane(Plane plane);

	public List<Plane> viewPlanes();

	public void updatePlane(Plane plane);

	public List<Plane> viewPlanesById(Plane plane);

//pilot
	/* public void insertPilot(Pilot pilot); */
	public List<Pilot> viewPilots();

	public void updatePilot(Pilot pilot);

	public List<Pilot> viewPilotById(Pilot pilot);

//hangar
	public void insertHangar(Hangar hangar);

	public List<Hangar> viewHangars();

	public List<Hangar> viewHangarById(Hangar hangar);
	public void updateHangar(Hangar hangar);

}
