package com.airport.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.airport.model.Admin;
import com.airport.model.Hangar;
import com.airport.model.Pilot;
import com.airport.model.Plane;

public class IAdminDaoImpl implements IAdminDao {

	@Override
	public void insertAdmin(Admin admin) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.save(admin);
		tx.commit();

	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean adminAthentication(Admin admin) {
		boolean isValid = false;
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query query = session.createQuery("from Admin where vendor_Id=?1 and password=?2");
		query.setParameter(1, admin.getVendor_Id());
		query.setParameter(2, admin.getPassword());
		List list = query.list();
		if (list != null && list.size() > 0) {
			isValid = true;
		}
		return isValid;
	}

	@Override
	public void insertPlane(Plane plane) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.save(plane);
		tx.commit();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<Plane> viewPlanes() {
		List<Plane> list = new ArrayList<Plane>();
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query query = session.createQuery("from Plane");
		List<Plane> l = query.list();
		for (Plane plane : l) {
			list.add(plane);
		}
		return list;
	}

	@SuppressWarnings({ "rawtypes" })
	@Override
	public void updatePlane(Plane plane) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		Query query = session.createQuery("update Plane set planeName=?1,source=?2,destination=?3 where planeId=?4");
		query.setParameter(1, plane.getPlaneName());
		query.setParameter(2, plane.getSource());
		query.setParameter(3, plane.getDestination());
		query.setParameter(4, plane.getPlaneId());
		query.executeUpdate();
		tx.commit();
		session.close();
	}

	/*
	 * @Override public void insertPilot(Pilot pilot) { Session session = new
	 * Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession(
	 * ); Transaction tx = session.beginTransaction(); session.save(pilot);
	 * tx.commit();
	 * 
	 * }
	 */

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<Plane> viewPlanesById(Plane plane) {
		List<Plane> list = new ArrayList<Plane>();
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query query = session.createQuery("from Plane where planeId=?1");
		query.setParameter(1, plane.getPlaneId());
		List<Plane> li = query.list();
		for (Plane plane2 : li) {
			list.add(plane2);
		}

		return list;

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<Pilot> viewPilots() {
		List<Pilot> list = new ArrayList<Pilot>();
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query query = session.createQuery("from Pilot");
		List<Pilot> l = query.list();
		for (Pilot pilot : l) {
			list.add(pilot);
		}
		return list;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void updatePilot(Pilot pilot) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		Query query = session
				.createQuery("update Pilot set pilotName=?1,age=?2,gender=?3,contactNumber=?4 where pilotId=?5");
		query.setParameter(1, pilot.getPilotName());
		query.setParameter(2, pilot.getAge());
		query.setParameter(3, pilot.getGender());
		query.setParameter(4, pilot.getContactNumber());
		query.setParameter(5, pilot.getPilotId());
		query.executeUpdate();
		tx.commit();
	}

	@Override
	public void insertHangar(Hangar hangar) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.save(hangar);
		tx.commit();

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<Hangar> viewHangars() {
		List<Hangar> list = new ArrayList<Hangar>();
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query query = session.createQuery("from Hangar");
		List<Hangar> l = query.list();
		for (Hangar hangar2 : l) {
			list.add(hangar2);
		}
		return list;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<Pilot> viewPilotById(Pilot pilot) {
		List<Pilot> list = new ArrayList<Pilot>();
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query query = session.createQuery("from Pilot where planeId=?1");
		query.setParameter(1, pilot.getPlaneId());
		List<Pilot> li = query.list();
		for (Pilot pilot2 : li) {
			list.add(pilot2);
		}

		return list;

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<Hangar> viewHangarById(Hangar hangar) {
		List<Hangar> list = new ArrayList<Hangar>();
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query query = session.createQuery("from Hangar where planeId=?1");
		query.setParameter(1, hangar.getPlaneId());
		List<Hangar> li = query.list();
		for (Hangar hangar2 : li) {
			list.add(hangar2);
		}
		return list;
		

	}

	@SuppressWarnings("rawtypes")
	@Override
	public void updateHangar(Hangar hangar) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		Query query = session.createQuery("update Hangar set hangarName=?1,hangarCapacity=?2,hangarLocation=?3,status=?4 where planeId=?5");
		query.setParameter(1,hangar.getHangarName());
		query.setParameter(2,hangar.getHangarCapacity() );
		query.setParameter(3, hangar.getHangarLocation());
		query.setParameter(4, hangar.getStatus());
		query.setParameter(5, hangar.getPlaneId());
		query.executeUpdate();
		tx.commit();
		session.close();
		
	}


}
