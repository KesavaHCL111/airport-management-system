package com.airport.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.airport.model.Manager;

public class IManagerDaoImpl implements IManagerDao {

	@Override
	public void inserManager(Manager manager) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.save(manager);
		tx.commit();
		
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean managerAthentication(Manager manager) {
		boolean isValid=false;
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query query=session.createQuery("from Manager where Manager_Id=?1 and password=?2");
		query.setParameter(1,manager.getManager_Id());
		query.setParameter(2, manager.getPassword());
	    List list=query.list();
	    if(list!=null && list.size()>0) {
	    	isValid=true;
	    } 
		return isValid;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List viewPlaneHangar() {
		List list = new ArrayList();
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query query = session.createQuery(
				"select p.planeId,p.planeName,h.hangarName,h.status from Plane p join Hangar h on p.planeId=h.planeId");
		List l = query.list();
		for (Object object : l) {
			list.add(object);
		}
		return list;
	}

}
