package com.airport.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name = "plane_id")
public class Pilot extends Plane {
	@Column(name = "pilot_id")
	private Integer pilotId;
	@Column(name = "pilot_name")
	private String pilotName;
	private Integer age;
	private String gender;
	@Column(name = "contact_number")
	private Long contactNumber;

	public Pilot() {
		// TODO Auto-generated constructor stub
	}

	public Pilot(String planeName,String source, String destination, Integer numberOfSeats, Integer pilotId, String pilotName, Integer age, String gender, Long contactNumber) {
		super(planeName,source,destination,numberOfSeats);
		this.pilotId = pilotId;
		this.pilotName = pilotName;
		this.age = age;
		this.gender = gender;
		this.contactNumber = contactNumber;
	}



	public Integer getPilotId() {
		return pilotId;
	}

	public void setPilotId(Integer pilotId) {
		this.pilotId = pilotId;
	}

	public String getPilotName() {
		return pilotName;
	}

	public void setPilotName(String pilotName) {
		this.pilotName = pilotName;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Long getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(Long contactNumber) {
		this.contactNumber = contactNumber;
	}

}
