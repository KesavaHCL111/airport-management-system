package com.airport.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Manager {
	private String first_Name;
	private String last_Name;
	private Integer age;
	private String gender;
	private Long contact_Number;
	@Id
	private String manager_Id;
	private String password;

	public Manager() {
		// TODO Auto-generated constructor stub
	}

	public Manager(String first_Name, String last_Name, Integer age, String gender, Long contact_Number,
			String manager_Id, String password) {
		super();
		this.first_Name = first_Name;
		this.last_Name = last_Name;
		this.age = age;
		this.gender = gender;
		this.contact_Number = contact_Number;
		this.manager_Id = manager_Id;
		this.password = password;
	}

	public String getFirst_Name() {
		return first_Name;
	}

	public void setFirst_Name(String first_Name) {
		this.first_Name = first_Name;
	}

	public String getLast_Name() {
		return last_Name;
	}

	public void setLast_Name(String last_Name) {
		this.last_Name = last_Name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Long getContact_Number() {
		return contact_Number;
	}

	public void setContact_Number(Long contact_Number) {
		this.contact_Number = contact_Number;
	}

	public String getManager_Id() {
		return manager_Id;
	}

	public void setManager_Id(String manager_Id) {
		this.manager_Id = manager_Id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
