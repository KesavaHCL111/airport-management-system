package com.airport.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name = "plane_id")
public class Hangar extends Plane {
	
	@Column(name = "hangar_name")
	private String hangarName;
	@Column(name = "hangar_capacity")
	private String hangarCapacity;
	@Column(name = " hangar_location")
	private String hangarLocation;
	private String status;

	public Hangar() {
		// TODO Auto-generated constructor stub
	}

	public Hangar(String planeName,String source, String destination, Integer numberOfSeats, String hangarName, String hangarCapacity, String hangarLocation, String status) {
		super(planeName,source,destination,numberOfSeats);
		this.hangarName = hangarName;
		this.hangarCapacity = hangarCapacity;
		this.hangarLocation = hangarLocation;
		this.status = status;
	}

	public String getHangarName() {
		return hangarName;
	}

	public void setHangarName(String hangarName) {
		this.hangarName = hangarName;
	}

	public String getHangarCapacity() {
		return hangarCapacity;
	}

	public void setHangarCapacity(String hangarCapacity) {
		this.hangarCapacity = hangarCapacity;
	}

	public String getHangarLocation() {
		return hangarLocation;
	}

	public void setHangarLocation(String hangarLocation) {
		this.hangarLocation = hangarLocation;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
