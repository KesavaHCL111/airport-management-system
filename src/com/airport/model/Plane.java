package com.airport.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)

public class Plane {
	@Id
	@Column(name = "plane_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer planeId;
	@Column(name = "plane_name")
	private String planeName;
	private String source;
	private String destination;
	@Column(name = " number_of_seats")
	private Integer numberOfSeats;

	public Plane() {
		// TODO Auto-generated constructor stub
	}

	public Plane( String planeName, String source, String destination, Integer numberOfSeats) {
		super();
		
		this.planeName = planeName;
		this.source = source;
		this.destination = destination;
		this.numberOfSeats = numberOfSeats;
	}

	
	public Integer getPlaneId() {
		return planeId;
	}

	public void setPlaneId(Integer planeId) {
		this.planeId = planeId;
	}

	public String getPlaneName() {
		return planeName;
	}

	public void setPlaneName(String planeName) {
		this.planeName = planeName;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Integer getNumberOfSeats() {
		return numberOfSeats;
	}

	public void setNumberOfSeats(Integer numberOfSeats) {
		this.numberOfSeats = numberOfSeats;
	}

}
